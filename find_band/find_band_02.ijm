// Using an imageTitle with fixed title and comments
imgTitle = "U2OS_1.tif";
run("Split Channels");
selectWindow("C1-"+imgTitle);
selectWindow("C2-"+imgTitle);

// Thershold and analyze connected components
run("Median...", "radius=8");
setAutoThreshold("Huang dark");
setOption("BlackBackground", true);
run("Convert to Mask");
run("Fill Holes");
run("Analyze Particles...", "size=1000-30000 clear exclude include add");

// Create band ROI and measure
roiManager("Select", 4);
run("Enlarge...", "enlarge=-10 pixel");
run("Make Band...", "band=10");
roiManager("Update");
selectWindow("C1-"+imgTitle);
run("Set Measurements...", "mean display redirect=None decimal=3");
roiManager("Measure");
