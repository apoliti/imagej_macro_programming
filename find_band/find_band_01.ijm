run("Split Channels");
selectWindow("C2-HK_1.tif");
run("Median...", "radius=8");
setAutoThreshold("Huang dark");
setOption("BlackBackground", true);
run("Convert to Mask");
run("Fill Holes");

// extra command to clean all previous data
roiManager("reset");

run("Analyze Particles...", "size=1000-30000 clear exclude include add");
roiManager("Select", 6);

run("Enlarge...", "enlarge=-10 pixel");
run("Make Band...", "band=10");
roiManager("Update");
selectWindow("C1-HK_1.tif");
run("Set Measurements...", "mean display redirect=None decimal=3");
roiManager("Measure");
