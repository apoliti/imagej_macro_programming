// Using an imageTitle using user defined functions
// String variables application
run("Close All");
open();
imgTitle = getTitle();
run("Split Channels");
selectWindow("C2-"+imgTitle);
// Thershold and analyze connected components
run("Median...", "radius=8");
setAutoThreshold("Huang dark");
setOption("BlackBackground", true);
run("Convert to Mask");
run("Fill Holes");
run("Analyze Particles...", "size=1000-30000 clear exclude include add");

// Create band ROI and measure
nrRois = roiManager("count");
run("Set Measurements...", "mean display redirect=None decimal=3");

for (iroi = 0; iroi < nrRois; iroi++){
	roiManager("Select", iroi);
	run("Enlarge...", "enlarge=-10 pixel");
	run("Make Band...", "band=10");
	roiManager("Update");
	selectWindow("C1-"+imgTitle);
	roiManager("Measure");
}
