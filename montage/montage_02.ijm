// Process with some additional contrast adjustments
run("Properties...", "channels=2 slices=1 frames=1 unit=um pixel_width=0.13 pixel_height=0.13 voxel_depth=1");
Stack.setChannel(1); // This is the first channel. IJ macro has often index base 1. Jave has index base 0
run("Green");
run("Enhance Contrast", "saturated=0.35");
Stack.setChannel(2); // This is the first channel. IJ macro has often index base 1. Jave has index base 0
run("Magenta");
run("Enhance Contrast", "saturated=0.35");
run("Make Montage...", "columns=2 rows=1 scale=0.25 border=2");
run("Scale Bar...", "width=10 height=4 font=14 color=White background=None location=[Lower Right] bold hide");