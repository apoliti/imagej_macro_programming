run("Properties...", "channels=2 slices=1 frames=1 unit=um pixel_width=0.13 pixel_height=0.13 voxel_depth=1");
run("Median...", "radius=2 stack");
run("Make Montage...", "columns=2 rows=1 scale=0.25 border=2");
run("Scale Bar...", "width=10 height=4 font=14 color=White background=None location=[Lower Right] bold hide");