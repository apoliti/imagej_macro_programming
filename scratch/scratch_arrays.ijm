// declare an empty array
a = newArray(3); // array of size 3
a[0] = 1;
print("value in array " + a[0]); // index start at 0
// length of an array
a.length;
print(a.length);
print("value in array " + a[2]); // index start at 0 and finishes at length-1

// declare an array with values
a = newArray(1,2,3);
print(a[1]);

// Loop through arrays
for(i = 0; i < a.length; i++)
	print("value"+i + " " + a[i]);
	
	

// You can pass arrays to some functions 
roiManager("select", newArray(0,2,4));

// You can get Arrays as output
path = getDirectory("Choose a Directory");
print(path);
list_files = getFileList(path);
for (i = 0; i < list_files.length; i++) {
	print(list_files[i]);
}

