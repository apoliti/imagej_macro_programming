#@ Integer (label="Median filter size") px_median
#@ Float (label="minimal area", value = 1000) minArea
#@ Float (label= "maximal area", value = 30000 ) maxArea
#@ String (label = "a string") astring
#@ String (label = "Threshold method", choices = {"Otsu", "Huang"}, style = listbox) thrMethod
#@ File[] (label = "select files", style = "files") listofFiles

print(px_median);
print(astring);
for(i = 0; i<listofFiles.length; i++){
	print(listofFiles[i]);
}
//print(listofFiles);
//#@ File (label = "Choose a file", style = "file") afile // single file
//#@ File (label = "Choose a directory", style = "directory") adir // single directory
//#@ File[]  (label="select files or folders", style="both") listOfPaths
