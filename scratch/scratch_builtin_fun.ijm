// Functions can return something
print(getTitle());
imgTitle = getTitle();
print(imgTitle);

// Some functions accept arguments
print("hello"); // hello is the argument
val = getPixel(264,426);
print("Pixel at 264 426 = " + val);

// Some functions change value of their arguments
getDimensions(width, height, channels, slices, frames);
print("W " + width + " H " + height);

// Function may have a composite name
Stack.getPosition(channel, slice, frame); // position that I am viewing now

