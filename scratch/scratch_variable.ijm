// this is a comment

// String variables
var1 = "Cup";
var2 = "cake";
var3 = var1 + var2;
print(var3); // print to log
print(var1 + "board");
//var3  = var1*var2: // this does not make sense
//print(var3);

// Number variables
num1 = 5;
num2 = 2;
sum = num1+num2; 
prod = num1*num2;
print("This is the sum " + sum);
sum_string = "This is the sum " + sum; // this is not anymore a number
prod = num1*num2;
print(num1 + " * " + num2 + " = "  + prod);

