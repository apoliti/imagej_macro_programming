//How to repeat statement?
print("Hello");
print("Hello");
print("Hello");
print("Hello");

// Use a for loop!
// i is a variable. This is the index of the loop
// i++ <=> i = i+1, add a number for each loop
// i will vary between 0 and 9
for(i = 0; i < 10; i++) {
	print(i);
	print("Hello");
}

// How to apply it to ROIs
nrRois = roiManager("count");
for (iroi = 0; iroi < nrRois; iroi++){
	roiManager("select", iroi);
	run("Rotate...", "  angle=90");
	roiManager("update");

	
}
